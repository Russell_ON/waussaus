<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Sauce;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function show($slug){


        if($slug === 'overmij'){

            $page = Page::findBySlug($slug)->with('blocks')->firstOrFail();


        }elseif($slug === 'products'){

            $page = Page::findBySlug($slug)->firstOrFail();

        }elseif($slug === 'contact'){

            $page = Page::findBySlug($slug)->firstOrFail();


        }

        return $page;
    }

    public function home(){
        $page = Page::findBySlug('home')->with('blocks')->firstOrFail();
        // dd($page);

        $sauces = Sauce::limit(4)->get();
        return view('welcome',
            ['page' => $page,
                'sauces' => $sauces,

            ]
        );
    }

}
