<?php

namespace App\Http\Controllers;

use App\Http\Controllers\PageController;
use App\Models\Sauce;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){


        $sauces = Sauce::get();
        $page = new PageController;
        $page = $page->show('products');

        return view('index',[
            'page' => $page,
            'sauces' => $sauces,
        ]);
    }

    public function show($sauce){
        $page = new PageController;
        $page = $page->show('products');
        $sauces = Sauce::limit(4)->latest()->get();

        $sauce = Sauce::findOrFail($sauce);

        return view('show',
            [   'page' => $page,
                'sauce' => $sauce,
                'sauces' => $sauces,
            ]
        );
    }
}
