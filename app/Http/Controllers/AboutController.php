<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(){

        $page = new PageController;
        $page = $page->show('overmij');


        return view('overmij',
            ['page' => $page]
        );
    }
}
