<?php

namespace App\Http\Controllers;

use App\Models\Sauce;
use Illuminate\Http\Request;

class SauceController extends Controller
{
    public function create(){
        return view('dashboard.saucecreate');
    }

    public function store(Request $request){

        //validation for form
        $this->validate($request, [
            'title' => 'required|max:255',
            'price' => 'required|numeric',
            // 'description' => 'required',
            // 'ingredients' => 'required|max:255',
        ]);

        //validation for image
        if($request->hasFile('image','bannerimage')){
            $this->validate($request,[
                'image' => 'required|file|image',
                'bannerimage' => 'required|file|image',
            ]);
        }

        $imagePath = $request->image->store('uploads', 'public');
        $bannerPath = $request->bannerimage->store('uploads', 'public');

        Sauce::create([
            'title' => $request->title,
            'price' => $request->price,
            'description' => $request->description,
            'ingredients' => $request->ingredients,
            'extra_information' => $request->extra_information,
            'image' => $imagePath,
            'bannerimage' => $bannerPath,
        ]);

        return redirect()->route('product.index');

    }

    public function edit($sauce){

        $sauce = Sauce::find($sauce);

        return view('dashboard.sauceedit', [
            'sauce' => $sauce,
        ]);
    }

    public function update(Request $request,$sauce){

        // dd($request);
        if($request->hasFile('image','bannerimage')){
            $this->validate($request,[
                // 'bannerimage' => 'required|file|image',
            ]);
        }

        // $bannerPath = $request->bannerimage->store('uploads', 'public');


        Sauce::where('id', $sauce)->update([
            'title' => $request->title,
            'price' => $request->price,
            'description' => $request->description,
            'ingredients' => $request->ingredients,
            'extra_information' => $request->extra_information,
            // 'bannerimage' => $bannerPath,
        ]);

        return redirect()->route('product.show', [$sauce]);
    }
}
