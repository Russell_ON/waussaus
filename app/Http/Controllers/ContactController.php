<?php

namespace App\Http\Controllers;

use App\Mail\AdminMail;
use App\Mail\Contact as MailContact;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index(){

        $page = new PageController;
        $page = $page->show('contact');
        return view('contact',
            ['page' => $page]
        );
    }

    public function store(Request $request){

        // dd($request);


        //validation for form
        // $this->validate($request, [
        //     'name' => 'required|max:255',
        //     'email' => 'required|max:255|email',
        //     'number' => 'required|max:255|numeric',
        //     'message' => 'required|max:255|',
        // ]);

        Contact::create([
            'name' => $request->name,
            'email' => $request->email,
            'number' => $request->number,
            'message' => $request->message,
        ]);

        $details =[
            'name' => $request->name,
            'email' => $request->email,
            'number' => $request->number,
            'message' => $request->message,
        ];


        Mail::to($request->email)->send(new MailContact);
        Mail::to('iloyaltiger@gmail.com')->send(new AdminMail($details));



        // Mail::send('emails.admin', array(
        //     'name' => $request->get('name'),
        //     'email' => $request->get('email'),
        //     'number' => $request->get('number'),
        //     'message' => $request->get('message'),
        // ), function($message) use ($request){
        //     $message->from($request->email);
        //     $message->to('iloyaltiger@gmail.com', 'Hello Admin')->subject($request->get('subject'));
        // });

        return redirect()->route('contact');
    }

    public function delete(Contact $contact){

        $contact->delete();

        return redirect()->route('dashboard');
    }
}
