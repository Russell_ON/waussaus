<?php

namespace App\Http\Controllers;

use App\Models\Block;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    public function store(Request $request, $page){
        dd($request);
    }

    public function update(Request $request, $block){

        Block::where('id', $block)->update([
            'content' => $request->content,
        ]);

        return redirect()->route('home');

    }
}

