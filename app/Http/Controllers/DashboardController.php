<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Contact;
use App\Models\Page;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function create(){
        return view('createpage');
    }

    public function index(){

        $pages = Page::get();
        $contacts = Contact::latest()->get();


        return view('dashboard.index', [
            'pages' => $pages,
            'contacts' => $contacts,
        ]);
    }
    // public function show($id){

    //     $page = Page::findOrFail($id);

    //     return view('page.show', [
    //         'page' => $page,
    //     ]);
    // }

    public function show($block){

        $block = Block::findOrFail($block);

        return view('dashboard.show', [
            'block' => $block,
        ]);
    }

    public function edit($page){

        $page = Page::find($page);

        return view('dashboard.edit', [
            'page' => $page,
        ]);
    }

    public function update(Request $request, $page){

        Page::where('id', $page)->update([
            'page_title' => $request->page_title,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
            'meta_title' => $request->meta_title,
        ]);

        return redirect()->route('dashboard');

    }


    public function delete(Page $page){

        $page->delete();

        return redirect()->route('dashboard');
    }

    public function store(Request $request){

        //validation for form
        $this->validate($request, [
            'page_title' => 'required|max:255',
        ]);

        Page::create([
            'page_title' => $request->page_title,
            'meta_keywords' => $request->meta_keywords,
            'meta_description' => $request->meta_description,
            'meta_title' => $request->meta_title,
        ]);

        return redirect()->route('dashboard');
    }
}
