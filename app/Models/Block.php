<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    use HasFactory;

    protected $fillable = ['content', 'block_number', 'page_id'];

    public function page(){
        return $this->belongsTo(Page::class);
    }
}
