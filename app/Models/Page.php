<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = ['page_title', 'meta_keywords', 'meta_description', 'meta_title'];

    public function blocks(){
        return $this->hasMany(Block::class);
    }

    public static function findBySlug($slug){
        return static::where('page_title', $slug);
    }
}
