<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\BlockController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SauceController;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[PageController::class, 'home'])->name('home');
Route::get('/sauzen',[ProductController::class, 'index'])->name('product.index');
Route::get('/overmij', [AboutController::class, 'index'])->name('overmij');
Route::get('/sauzen/{sauce}', [ProductController::class, 'show'])->name('product.show');
Route::get('/contact', [ContactController::class, 'index'])->name('contact');


Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');

Route::prefix('dashboard')->group(function () {

    Route::middleware(['auth'])->group(function () {

        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
        Route::get('/createpage', [DashboardController::class, 'create'])->name('create.page');

        Route::get('/createsauce', [SauceController::class, 'create'])->name('sauce.create');
        Route::post('/createsauce', [SauceController::class, 'store'])->name('sauce.store');

        Route::get('/edit/page/{page}', [DashboardController::class, 'edit'])->name('dashboard.edit');
        Route::get('/edit/sauzen/{sauce}', [SauceController::class, 'edit'])->name('sauce.edit');

        Route::put('/edit/page/{page}', [DashboardController::class, 'update'])->name('page.update');
        Route::put('/edit/sauzen/{sauce}', [SauceController::class, 'update'])->name('sauce.update');


        Route::post('/createpage', [DashboardController::class, 'store'])->name('page.store');
        Route::delete('/delete/page/{page}', [DashboardController::class, 'delete'])->name('page.delete');
        Route::delete('/delete/contact/{contact}', [ContactController::class, 'delete'])->name('contact.delete');

        Route::get('/page/{block}', [DashboardController::class, 'show'])->name('page.edit');


        Route::put('/page/{block}', [BlockController::class, 'update'])->name('block.update');
        Route::post('/page/{page}/store', [BlockController::class, 'store'])->name('block.store');
    });
});

// Route::get('/dashboard', function () {
    //     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
