<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">


                        <div class="w-full bg-white rounded shadow-lg p-8 m-4">
                          <h1 class="text-3xl font-bold text-lg block w-full text-center text-grey-darkest mb-6">Voeg een saus toe</h1>

                          <form class="mb-4" action="{{ route('sauce.store') }}" method="POST" enctype="multipart/form-data">

                            @csrf
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="title">Saus Titel</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" name="title">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="price">Prijs</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" name="price">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="description">Omschrijving</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" name="description">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="ingredients">Ingredienten</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" name="ingredients">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="extra_information">Extra Informatie</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" name="extra_information">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="image">Afbeelding</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="file" name="image">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="bannerimage">Banner Image</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="file" name="bannerimage">
                            </div>

                            <button type="submit" class="w-full h-12 px-6 text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800">Voeg saus toe</button>
                          </form>

                        </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
