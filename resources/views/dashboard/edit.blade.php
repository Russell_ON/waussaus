<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="flex items-center h-screen w-full bg-teal-lighter">
                        <div class="w-full bg-white rounded shadow-lg p-8 m-4">
                          <h1 class="text-3xl font-bold text-lg block w-full text-center text-grey-darkest mb-6">Wijzig een Pagina</h1>

                          <form class="mb-4" action="{{ route('page.update', $page->id) }}" method="POST">

                            @method('put')
                            @csrf

                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="page_title">Pagina Titel</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" value="{{$page->page_title}}" name="page_title">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="meta_keywords">Keywords</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" value="{{$page->meta_keywords}}" name="meta_keywords">
                            </div>
                            <div class="flex flex-col mb-4">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="meta_description">Description</label>
                              <textarea class="border py-2 px-3 text-grey-darkest" name="meta_description" cols="30" rows="5">{{ $page->meta_description }}</textarea>
                            </div>
                            <div class="flex flex-col mb-6">
                              <label class="mb-2 font-bold text-lg text-grey-darkest" for="meta_title">Meta Title</label>
                              <input class="border py-2 px-3 text-grey-darkest" type="text" value="{{$page->meta_title}}" name="meta_title">
                            </div>
                            <button type="submit" class="w-full h-12 px-6 text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800">Wijzig pagina info</button>
                          </form>

                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
