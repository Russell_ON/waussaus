<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 bg-white border-b border-gray-200">
                    <h1 class="font-bold text-4xl"><span class="font-thin">Verander de content van block </span>{{ $block->block_number }}</h1>
                </div>
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('block.update', [$block->id]) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <textarea name="content" id="content" cols="30" rows="10">{{ $block->content }}</textarea>

                        <button type="submit" class="w-full h-12 px-6 text-indigo-100 transition-colors duration-150 bg-indigo-700 rounded-lg focus:shadow-outline hover:bg-indigo-800">Block Aanpassen</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.tiny.cloud/1/ggfv0r19pczlr5yajezco0uoi1a592u0xkpohcwt9znac63k/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#content',
        })
    </script>
</x-app-layout>
