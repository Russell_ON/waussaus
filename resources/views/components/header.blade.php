<header>
    <nav class="scroll">
        <div class="nav-center">
            <div class="nav-header">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/waus-saus_logo_kleur-re.png') }}" alt="">
                </a>
            </div>
            <div class="links-container">
                <ul class="links">
                    <li>
                        <a href="{{ route('home') }}" class="scroll-links">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('overmij') }}" class="scroll-links">Over mij</a>
                    </li>
                    <li>
                        <a href="{{ route('product.index') }}" class="scroll-links">Sauzen</a>
                    </li>
                    <li>
                        <a href="{{ route('contact') }}" class="scroll-links">Contact</a>
                    </li>
                    @auth
                    <li>
                        <a class="dashboard" href="{{ route('dashboard') }}" class="scroll-links">Dashboard</a>
                    </li>
                    @endauth
                </ul>
            </div>
            <div class="toggle-button">
                <i id="fa" class="fas fa-bars"></i>
            </div>
        </div>

        <div class="nav-mobile">
            <ul class="list-mobile">
                <li>
                    <a href="{{ route('home') }}" class="scroll-links">Home</a>
                </li>
                <li>
                    <a href="{{ route('overmij') }}" class="scroll-links">Over mij</a>
                </li>
                <li>
                    <a href="{{ route('product.index') }}" class="scroll-links">Sauzen</a>
                </li>
                <li>
                    <a href="{{ route('contact') }}" class="scroll-links">Contact</a>
                </li>
                @auth
                <li>
                    <a class="dashboard" href="{{ route('dashboard') }}" class="scroll-links">Dashboard</a>
                </li>
                @endauth
            </ul>
        </div>
    </nav>
</header>
